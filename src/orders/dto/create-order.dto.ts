export class CreateOrderDto {
  orderItem: {
    productId: number;
    qty: number;
  }[];
  userId: number;
}
